package sample;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXToggleButton;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;


public class Controller implements Initializable {

    public static Player player;
    private static int channelId = 1;
    private static String musicLink;
    private static String[] channels = {"IloveRadio", "ILove2Dance", "ILoveTheDJ", "ILoveTheSun"};
    private static float volume = 35;
    private static WebEngine webEngine;
    private static boolean active = false;
    private BufferedInputStream inputStream;
    @FXML
    private JFXToggleButton btn_toggle;
    @FXML
    private JFXComboBox comboBox_Channel;
    @FXML
    private JFXSlider slider_volume;
    @FXML
    private WebView webviev;
    @FXML
    private Text mytext, mytext2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        comboBox_Channel.getItems().addAll(channels);
        comboBox_Channel.getSelectionModel().select(0);
        mytext.setFont(Font.loadFont("https://www.iloveradio.de/typo3conf/ext/ep_channel/Resources/Public/fonts/ILoveRadioBold.ttf", 22));
        mytext2.setFont(Font.loadFont("https://www.iloveradio.de/typo3conf/ext/ep_channel/Resources/Public/fonts/ILoveRadio.ttf", 22));
        initWebEngine();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                changeUI(channelId);
            }
        }, 0, 5000);
    }

    @FXML
    public void onDrag(MouseEvent event) {
        if (event.getSource() == slider_volume) {
            volume = (float) slider_volume.getValue();
            try {
                player.setGain(volume);
            } catch (NullPointerException e) {
            }

        }
    }

    @FXML
    private void onButtonClick(ActionEvent event) {
        if (event.getSource() == btn_toggle) {
            if (btn_toggle.isSelected()) {
                onEnable();
            } else {
                onDisable();
            }
        }
    }

    @FXML
    private void onDragDone(MouseEvent event) {
        if (event.getSource() == slider_volume) {
            volume = (float) slider_volume.getValue();
            try {
                player.setGain(volume);
            } catch (NullPointerException ignored) {
            }
        }
    }

    @FXML
    private void onMenuItem(ActionEvent event) {
        System.out.println(slider_volume.getValue());
        if (event.getSource() == comboBox_Channel) {
            switch (comboBox_Channel.getSelectionModel().getSelectedItem().toString()) {
                case "ILove2Dance":
                    channelId = 2;
                    break;
                case "ILoveRadio":
                    channelId = 1;
                    break;
                case "ILoveTheDJ":
                    channelId = 4;
                    break;
                case "ILoveTheSun":
                    channelId = 15;
                    break;
                default:
                    channelId = 1;
                    break;
            }
        }
        refreshLink(channelId);
        if (active) {
            onDisable();
            onEnable();
        } else {
            onDisable();
        }
        System.out.println(active);
    }

    private void playMusic(String url) {
        try {
            if (player == null && active && inputStream == null) {
                inputStream = new BufferedInputStream(new URL(url).openStream());
                player = new Player(inputStream);
                player.setGain(volume);
                player.play();
            }
        } catch (IOException | JavaLayerException e) {
            e.printStackTrace();
        }
    }

    private void refreshLink(int id) {
        musicLink = "http://stream01.iloveradio.de/iloveradio" + channelId + ".mp3";
    }

    private void onDisable() {
        changeUI(channelId);
        if (player != null) {
            player.close();
            player = null;
        }
        active = false;
        try {
            if(inputStream == null) return;
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        inputStream = null;
    }

    private void onEnable() {
        refreshLink(channelId);
        new Thread(() -> playMusic(musicLink)).start();
        active = true;
    }

    private void initWebEngine() {
        WebView webView = webviev;
        webEngine = webView.getEngine();
        webEngine.setJavaScriptEnabled(true);
        webEngine.setUserAgent("BaguetteLauncher (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 BaguetteLauncher/25.0");
    }

    private void changeUI(int channelId) {
        try {

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(System.currentTimeMillis());
            URL urlXML = new URL("https://www.iloveradio.de/typo3conf/ext/ep_channel/Scripts/playlistPerChannel.php?tx_epchannel_channellist[from]=latest&tx_epchannel_channellist[till]=latest&tx_epchannel_channellist[date]=" + formatter.format(date) + "&tx_epchannel_channellist[channel]=" + channelId);


            URLConnection conn = urlXML.openConnection();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(conn.getInputStream());

//            xform.transform(new DOMSource(doc), new StreamResult(System.out));

            mytext.setText(doc.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getNodeValue().replace("&amp;", ""));
            mytext2.setText(doc.getFirstChild().getFirstChild().getChildNodes().item(1).getFirstChild().getNodeValue().replace("&amp;", ""));
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    webEngine.load("https://www.iloveradio.de/" + doc.getFirstChild().getFirstChild().getChildNodes().item(2).getFirstChild().getNodeValue());
                }
            });

        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }
}
